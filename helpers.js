import { fileURLToPath } from 'url'
import { dirname } from 'path'
import { existsSync, appendFile, writeFile, unlink, readFileSync } from 'fs'

const __filename = fileURLToPath(import.meta.url)

export const __dirname = dirname(__filename)

export const writeToFile = (filePath, data) => {
  if (existsSync(filePath)) {
    appendFile(filePath, data.toString() + '\n', (err) => {
      if (err) throw err
    })
    console.log('Adding successfully')
  } else {
    writeFile(filePath, data.toString() + '\n', (err) => {
      if (err) throw err
    })
    console.log('File was created')
    console.log('Adding successfully')
  }
}

export const deleteFile = (filePath) => {
  if (existsSync(filePath)) {
    unlink(filePath, (err) => {
      if (err) throw err
    })
    console.log('File was deleted')
  } else {
    console.log('File not exist')
  }
}

export const cleanFile = (filePath) => {
  if (existsSync(filePath)) {
    writeFile(filePath, '', (err) => {
      if (err) throw err
    })
    console.log('File was cleaned')
  } else {
    console.log('File not exist')
  }
}

export const readFromFile = (filePath) => {
  if (existsSync(filePath)) {
    let content = readFileSync(filePath, 'utf-8')
    console.log(content)
  } else {
    console.log('File not exist')
  }
}
