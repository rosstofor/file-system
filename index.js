import { __dirname, writeToFile, deleteFile, cleanFile, readFromFile } from './helpers.js'

const file = 'file.txt'
const filePath = `${__dirname}/${file}`
let data = process.argv.slice(2)

// If the arguments contain service variable
if (process.argv.includes('-delete')) {
  deleteFile(filePath)
  process.exit()
}
if (process.argv.includes('-clean')) {
  cleanFile(filePath)
  process.exit()
}
if (process.argv.includes('-read')) {
  readFromFile(filePath)
  process.exit()
}

writeToFile(filePath, data)
